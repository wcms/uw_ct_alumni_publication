<?php

/**
 * @file
 * uw_ct_alumni_publication.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_alumni_publication_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_alumni_publications content'.
  $permissions['create uw_alumni_publications content'] = array(
    'name' => 'create uw_alumni_publications content',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_alumni_publications content'.
  $permissions['delete any uw_alumni_publications content'] = array(
    'name' => 'delete any uw_alumni_publications content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_alumni_publications content'.
  $permissions['delete own uw_alumni_publications content'] = array(
    'name' => 'delete own uw_alumni_publications content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_alumni_publications content'.
  $permissions['edit any uw_alumni_publications content'] = array(
    'name' => 'edit any uw_alumni_publications content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_alumni_publications content'.
  $permissions['edit own uw_alumni_publications content'] = array(
    'name' => 'edit own uw_alumni_publications content',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'search uw_alumni_publications content'.
  $permissions['search uw_alumni_publications content'] = array(
    'name' => 'search uw_alumni_publications content',
    'roles' => array(),
    'module' => 'search_config',
  );

  return $permissions;
}
