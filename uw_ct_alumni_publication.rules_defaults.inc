<?php

/**
 * @file
 * uw_ct_alumni_publication.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_ct_alumni_publication_default_rules_configuration() {
  $items = array();
  $items['rules_after_submit_alumni_publications'] = entity_import('rules_config', '{ "rules_after_submit_alumni_publications" : {
      "LABEL" : "After submit alumni publications",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "workbench_moderation" ],
      "ON" : { "node_insert" : [] },
      "IF" : [
        { "node_is_of_type" : {
            "node" : [ "node" ],
            "type" : { "value" : { "uw_alumni_publications" : "uw_alumni_publications" } }
          }
        }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "node:field-alumni-image:alt" ], "value" : "Book cover" } },
        { "redirect" : { "url" : "alumni-publications\\/write-university-waterloo" } },
        { "mail" : {
            "to" : "[node:field-alumni-email]",
            "subject" : "Alumni publication form submitted",
            "message" : "You have successfully submitted your alumni publication form for review.",
            "language" : [ "" ]
          }
        },
        { "mail" : {
            "to" : "communications@alumni.uwaterloo.ca, records@alumni.uwaterloo.ca",
            "subject" : "Write On! submission received",
            "message" : "A Write On! submission has been received. Please review and approve the submission.[node:url]\\r\\n\\r\\nTitle: [node:title]\\r\\nFirst name: [node:field-alumni-fname]\\r\\nLast name: [node:field-alumni-lname]\\r\\nStudent ID: [node:field_alumni_stuid]\\r\\nFaculty: [node:field_alumni_faculty]\\r\\nCampus: [node:field_alumni_campus]\\r\\nDegree and years: [node:field-alumni-degree]\\r\\nDaytime telephone: [node:field-alumni-phone]\\r\\nEmail address: [node:field-alumni-email]\\r\\nType of book: [node:field-alumni-booktype]\\r\\nCo-authors: [node:field_alumni_co]\\r\\nYear of publication (or year of most recent reprint): [node:field_alumni_pubyear]\\r\\nComments: [node:field_alumni_comment]",
            "language" : [ "node:type" ]
          }
        },
        { "workbench_moderation_set_state_during_save" : { "node" : [ "node" ], "moderation_state" : "needs_review" } }
      ]
    }
  }');
  return $items;
}
