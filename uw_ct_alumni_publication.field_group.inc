<?php
/**
 * @file
 * uw_ct_alumni_publication.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_alumni_publication_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_alumni_book|node|uw_alumni_publications|form';
  $field_group->group_name = 'group_alumni_book';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_alumni_publications';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your book',
    'weight' => '1',
    'children' => array(
      0 => 'field_alumni_booktype',
      1 => 'field_alumni_co',
      2 => 'field_alumni_pubyear',
      3 => 'field_alumni_image',
      4 => 'field_alumni_comment',
      5 => 'title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-alumni-book field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_alumni_book|node|uw_alumni_publications|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_alumni_info|node|uw_alumni_publications|form';
  $field_group->group_name = 'group_alumni_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_alumni_publications';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your contact information',
    'weight' => '0',
    'children' => array(
      0 => 'field_alumni_fname',
      1 => 'field_alumni_lname',
      2 => 'field_alumni_stuid',
      3 => 'field_alumni_faculty',
      4 => 'field_alumni_campus',
      5 => 'field_alumni_degree',
      6 => 'field_alumni_phone',
      7 => 'field_alumni_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => ' group-alumni-info field-group-fieldset',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_alumni_info|node|uw_alumni_publications|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Your book');
  t('Your contact information');

  return $field_groups;
}
