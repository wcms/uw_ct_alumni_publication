<?php

/**
 * @file
 * uw_ct_alumni_publication.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_alumni_publication_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'alumni_publication_block';
  $context->description = 'Display alumni publication author and book search';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'alumni-publications/write-university-waterloo' => 'alumni-publications/write-university-waterloo',
        'node/add/uw-alumni-publications' => 'node/add/uw-alumni-publications',
        'alumni-publications/write-university-waterloo/donate-your-book' => 'alumni-publications/write-university-waterloo/donate-your-book',
        'book-search' => 'book-search',
        'alumni-publications/A' => 'alumni-publications/A',
        'alumni-publications/B' => 'alumni-publications/B',
        'alumni-publications/C' => 'alumni-publications/C',
        'alumni-publications/D' => 'alumni-publications/D',
        'alumni-publications/E' => 'alumni-publications/E',
        'alumni-publications/F' => 'alumni-publications/F',
        'alumni-publications/G' => 'alumni-publications/G',
        'alumni-publications/H' => 'alumni-publications/H',
        'alumni-publications/I' => 'alumni-publications/I',
        'alumni-publications/J' => 'alumni-publications/J',
        'alumni-publications/K' => 'alumni-publications/K',
        'alumni-publications/L' => 'alumni-publications/L',
        'alumni-publications/M' => 'alumni-publications/M',
        'alumni-publications/N' => 'alumni-publications/N',
        'alumni-publications/O' => 'alumni-publications/O',
        'alumni-publications/P' => 'alumni-publications/P',
        'alumni-publications/Q' => 'alumni-publications/Q',
        'alumni-publications/R' => 'alumni-publications/R',
        'alumni-publications/S' => 'alumni-publications/S',
        'alumni-publications/T' => 'alumni-publications/T',
        'alumni-publications/U' => 'alumni-publications/U',
        'alumni-publications/V' => 'alumni-publications/V',
        'alumni-publications/W' => 'alumni-publications/W',
        'alumni-publications/X' => 'alumni-publications/X',
        'alumni-publications/Y' => 'alumni-publications/Y',
        'alumni-publications/Z' => 'alumni-publications/Z',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-33f436756c0cbb4a15c6e3932dbce5db' => array(
          'module' => 'views',
          'delta' => '33f436756c0cbb4a15c6e3932dbce5db',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'views-e9c962d1da48a611ae1f176dac3c46f9' => array(
          'module' => 'views',
          'delta' => 'e9c962d1da48a611ae1f176dac3c46f9',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display alumni publication author and book search');
  $export['alumni_publication_block'] = $context;

  return $export;
}
